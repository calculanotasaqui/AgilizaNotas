﻿using System;

Console.WriteLine("Olá Professor!\n me chamo handyman e vou deixar seu dia menos burocratico!\n");
Console.WriteLine("Como posso chama-lo?");

// Lendo o nome do usuario
string userName = Console.ReadLine();

// verificando se digitou alguma coisa valida
if (!string.IsNullOrWhiteSpace(userName))
{
    // colocando a primeira letra maiuscula
    string nomeFormatado = char.ToUpper(userName[0]) + userName.Substring(1).ToLower();
    // Saudações
    Console.WriteLine("\nFala Professor " + userName + "! Eu estou aqui para ajuda-lo.\n\n");
}
else
{
    Console.WriteLine("\nOps! Parece que você não digitou um nome válido.");
        }

// Solicitando o nome do estudante
Console.WriteLine("Professor " + userName + ", qual o nome do aluno que nós vamos avaliar?");

// Recebendo o nome do estudante
string studentName = Console.ReadLine();
if (!string.IsNullOrWhiteSpace(studentName))
{
    // colocando a primeira letra maiuscula
    string studentNameformated = char.ToUpper(userName[0]) + userName.Substring(1).ToLower();
    // Saudações
    Console.WriteLine("\nOlá Professor " + userName + "! Eu estou aqui para ajuda-lo.\n\n");
}
else
{
    Console.WriteLine("\nOps! Parece que você não digitou um nome válido.");
        }

Console.WriteLine("Estudante " + studentName + ", certo e o numero de matricula dela?");

string studentNumber = Console.ReadLine();

Console.Write("Certo, então o nome dela é " + studentName + ", e o numero de matricula dela é " + studentNumber);

Console.WriteLine("\n\nProfessor " + userName + ", de qual materia vamos começar?");
string course1Name = Console.ReadLine();

Console.WriteLine(course1Name + ", certo, e qual foi a nota da primeira avaliação?");
decimal notamateria1avaliação1Input;
do
{
    Console.WriteLine(course1Name + ", certo, e qual foi a nota da primeira avaliação?");
} while (!decimal.TryParse(Console.ReadLine(), out notamateria1avaliação1Input) || notamateria1avaliação1Input < 0);

Console.WriteLine("\nQual foi a nota da segunda avaliação?");
decimal notamateria1avaliação2Input;
do
{
    Console.WriteLine(course1Name + ", certo, e qual foi a nota da segunda avaliação?");
} while (!decimal.TryParse(Console.ReadLine(), out notamateria1avaliação2Input) || notamateria1avaliação2Input < 0);

Console.WriteLine("\nQual foi a nota da terceira avaliação?");
decimal notamateria1avaliação3Input;
do
{
    Console.WriteLine(course1Name + ", certo, e qual foi a nota da terceira avaliação?");
} while (!decimal.TryParse(Console.ReadLine(), out notamateria1avaliação3Input) || notamateria1avaliação3Input < 0);

Console.WriteLine("\nQual foi a nota da quarta avaliação?");
decimal notamateria1avaliação4Input;
do
{
    Console.WriteLine(course1Name + ", certo, e qual foi a nota da quarta avaliação?");
} while (!decimal.TryParse(Console.ReadLine(), out notamateria1avaliação4Input) || notamateria1avaliação4Input < 0);

decimal notafinalmateria1 = (notamateria1avaliação1Input + notamateria1avaliação2Input + notamateria1avaliação3Input + notamateria1avaliação4Input) / 4;
if (notafinalmateria1 > 7) {
    Console.WriteLine("O estudante " + studentName + " foi aprovado!");
}
else {
    Console.WriteLine("O estudante " + studentName + " foi reprovado");
}

string course2Name = "Algebra 101";
string course3Name = "Biology 101";
string course4Name = "Computer Science I";
string course5Name = "Psychology 101";

decimal course2Grade = 3;
decimal course3Grade = 3;
decimal course4Grade = 3;
decimal course5Grade = 4;

int course1Credit = 3;
int course2Credit = 3;
int course3Credit = 4;
int course4Credit = 4;
int course5Credit = 3;

Console.WriteLine($"\nEstudante:\t{studentName}\n");
Console.WriteLine($"\nMatricula:\t{studentNumber}\n");
Console.WriteLine("Course\t\t\t\tGrade\tCredit\tHours");
Console.WriteLine($"{course1Name}\t\t\t{notafinalmateria1}\t\t{course1Credit}");
Console.WriteLine($"{course2Name}\t\t\t{course2Grade}\t\t{course2Credit}");
Console.WriteLine($"{course3Name}\t\t\t{course3Grade}\t\t{course3Credit}");
Console.WriteLine($"{course4Name}\t\t{course4Grade}\t\t{course4Credit}");
Console.WriteLine($"{course5Name}\t\t\t{course5Grade}\t\t{course5Credit}");
